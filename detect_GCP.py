import numpy as np
import cv2
from PIL import Image
import cv2
import os
import pandas as pd

# to get final data in a tabular manner
output=pd.DataFrame()

# function gets area described by 3 points on a plane. Used later to filter out noisy white regions
def polygon_area(x,y):
    correction = x[-1] * y[0] - y[-1]* x[0]
    main_area = np.dot(x[:-1], y[1:]) - np.dot(y[:-1], x[1:])
    return 0.5*np.abs(main_area + correction)

folder= './Input_images'

for filename in os.listdir(folder):
    try:
        col = Image.open(os.path.join(folder,filename))
    except:
        continue
    
    # convert to Binary Black/White image to extract only white features
    gray = col.convert('L')
    bw = gray.point(lambda x: 0 if x<230 else 255, '1')
    bw.save("result_bw.png")
    
    # get contours of white regions
    im = cv2.imread('result_bw.png')
    imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
    ret,thresh = cv2.threshold(imgray,127,255,0)
    im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    
    # get only concave white regions 
    concave_cnt=[]
    for x in contours:
        if(not(cv2.isContourConvex(x))):
            concave_cnt.append(x)
    
    # get concave white regions with 3 corners
    cnt_3=[]    
    for cnt in concave_cnt:
        epsilon = 0.1*cv2.arcLength(cnt,True)
        approx = cv2.approxPolyDP(cnt,epsilon,True)

        if(len(approx)==3):
            cnt_3.append(approx)
    
    get_ar=[]
    get_ar=[x.reshape([3,2]).T for x in cnt_3]
    
    for g in enumerate(get_ar):
        s=[]
        if((polygon_area(g[1][0],g[1][1])>50) and (polygon_area(g[1][0],g[1][1])<200)):
            
            # getting 3 sides of triangle to get an approximation of a right triangle
            # the sides are set up in a way that idx gives the index of the point
            # opposite to the longest side. Which would be the GCP location in pixel value
            
            s.append(np.linalg.norm(g[1].T[1]-g[1].T[2]))
            s.append(np.linalg.norm(g[1].T[0]-g[1].T[2]))
            s.append(np.linalg.norm(g[1].T[0]-g[1].T[1]))
            max_s=max(s)
            idx=s.index(max(s))
            s.remove(max_s)
            
            # calculate margin for pythagoras theorem : if diff=0, then right angle is present
            diff=max_s-(sum([side**2 for side in s]))**0.5

            if(diff>0 and diff<0.45):
                print('{}: {}'.format(filename,str(g[1].T[idx])))
                temp=pd.DataFrame.from_dict({'File':filename, 'Co-ordinates':[g[1].T[idx]]})
                output=output.append(temp)
                
output.to_csv('output.csv')