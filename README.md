# SkylarkAssignment

Assignment to detect GCP in drone images.

## Related reading

My first approach was to look for a method similar to template matching however since I would want it to be size and rotational invariant, I had to rule it out. I then tried out Flann Feature mapping, which did not work out either as there aren't any explicit features in our GCP point. In the end I had to settle for making an algorithm which is rule based and tailored acording to the features of the GCP. A CNN could be trained for this task using perhaps using YOLO to detect GCP in images however there weren't enough training images for that task and so I had to drop the idea. 

Finally I decided to settle for the approach descrbed below. 

## My Approach

As explained in the assignment, I had to detect * white * L shaped figures in the image so my approach was as follows:

- Create a Binary Image with threshold set to around 230 to only keep white regions in the image
- Use openCv's `findContours()` to find contour points
- Use the obtained contour points of all shapes to filter out only concave shapes using not of `isContourConvex()` of openCv since the shape we wish to detect is concave and this would filter out a lot of unnecessary shapes
- Finally use `approxPolyDP()` with `epsilon = 0.1` to further limit contour points to only be ones through which a polygon could be constructed. This is essential as applying this function on L shape would give 3 corner points and we could further filter out more shapes
- Filter out all shapes with number of corners other 3
- Now we have a collection of 3-points in space of many different shapes, however we have to only choose those which resemble an L
- Emphasising on the right angle as the unique feature in the shape that I wish to detect, I applied pythagoras theorem and applied size threshold limitations through calculating the area so extremely tiny points get filtered out
- The resulting points were not always exactly at right angles so I kept a `0.5` margin of error. 
- The final points were the points opposite the longest side (hypotenuse) of the triangle made by the 3 points and hence must be the point of right angle. 

## Limitations

My algorithm is only working for certain images. Even though it is clearly detecting the correct pattern, it sometimes also detects false positives in some cases and in some cases it is not detecting either because of some obscurity or some colour abberations (less lighting making the GCP a shade darker than white).

The research and trying out initial algorithms took me around 2-3 days and 3 days to finally flesh out the final algorithm. Since I am a working individual it was a bit difficult to find a lot of time for the task, however I did the best I could.